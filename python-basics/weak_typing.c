#include <stdio.h>

int main(void)
{
	const char *str = "This is a string variable.\n";
	int n = 100;

	printf("%d\n", str + n);

	return 0;
}
